﻿using System;
using System.Windows.Input;

namespace StackOverflowException
{
    public class DelegateCommand<T> : ICommand where T : class
    {
        private readonly Predicate<T> canExecute;
        private readonly Action<T> execute;
        protected bool IsEnabled = true;

        public DelegateCommand(Action<T> execute, Predicate<T> canExecute = null)
        {
            this.canExecute = canExecute ?? (T => IsEnabled);
            this.execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecute((T) parameter);
        }

        public void Execute(object parameter)
        {
            execute((T) parameter);
        }

        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }

        protected virtual void OnCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            handler?.Invoke(this, EventArgs.Empty);
        }
    }
}