﻿namespace StackOverflowException
{
    public interface IWindow
    {
        object DataContext { get; set; } 
    }
}