﻿using System;
using System.Drawing;
using System.Windows;
using StructureMap;
using Image = System.Windows.Controls.Image;
using static System.Math;
using static System.Windows.Input.Mouse;

namespace StackOverflowException
{
    public sealed class MainWindowViewModel : BaseWindowViewModel, IMainWindowViewModel
    {
        public MainWindowViewModel(IMainWindow window, IContainer container) : base(window, container)
        {
            StartRecursiveCall = new DelegateCommand<object>(OnRecursiveCall);
        }

        private void OnRecursiveCall(object obj)
        {
            var image = (Image) obj;
            SaveBmp = getUnixEpochTime();
            ImageWidth = (int) Ceiling(image.Source.Width);
            ImageHeight = (int) Ceiling(image.Source.Height);
            var position = GetPosition((IInputElement) obj);
            using (var pacman = new Bitmap(WrongPacmanUri))
            {
                var x = (int) Ceiling(position.X) - 3;
                var y = (int) Ceiling(position.Y) - 3;
                floodFill8(x, y, Color.Red, pacman.GetPixel(x, y), pacman);
            }
            WrongPacmanUri = SaveBmp;
            SaveBmp = getUnixEpochTime();
        }

        public DelegateCommand<object> StartRecursiveCall { get; set; }

        private int recursiveCalls;

        public int RecursiveCalls
        {
            get { return recursiveCalls; }
            set
            {
                recursiveCalls = value;
                OnPropertyChanged();
            }
        }

        private string worngPacmanUri;

        public string WrongPacmanUri
        {
            get { return worngPacmanUri; }
            set
            {
                worngPacmanUri = value;
                OnPropertyChanged();
            }
        }

        #region private properties
        
        private int ImageWidth { get; set; }

        private int ImageHeight { get; set; }

        private string SaveBmp { get; set; }

        #endregion

        private string getUnixEpochTime()
        {
            return
                @"D:\VisualStudio\Desktop\StackOverflowException\StackOverflowException\StackOverflowException\Resources\FilledPacman" +
                (int) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + ".bmp";
        }

        private void floodFill8(int x, int y, Color newColor, Color oldColor, Bitmap pacman)
        {
            var pixelColor = pacman.GetPixel(x, y);
            if (x > 0 && x < ImageHeight && y > 0 && y < ImageWidth && pixelColor == oldColor &&
                pixelColor != newColor)
            {
                RecursiveCalls++;
                Console.WriteLine(RecursiveCalls);
                pacman.SetPixel(x, y, newColor);
                pacman.Save(SaveBmp);

                floodFill8(x + 1, y, newColor, oldColor, pacman);
                floodFill8(x - 1, y, newColor, oldColor, pacman);
                floodFill8(x, y + 1, newColor, oldColor, pacman);
                floodFill8(x, y - 1, newColor, oldColor, pacman);
                floodFill8(x + 1, y + 1, newColor, oldColor, pacman);
                floodFill8(x - 1, y - 1, newColor, oldColor, pacman);
                floodFill8(x - 1, y + 1, newColor, oldColor, pacman);
                floodFill8(x + 1, y - 1, newColor, oldColor, pacman);
            }
        }
    }
}