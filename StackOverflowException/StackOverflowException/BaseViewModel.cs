﻿using StructureMap;

namespace StackOverflowException
{
    public class BaseViewModel : BaseNotification, IViewModel
    {
        public BaseViewModel(IView view, IContainer container)
        {
            View = view;
            View.DataContext = view;
            Container = container;
        }

        public IView View { get; set; }

        public IContainer Container { get; set; }
    }
}