﻿using System.Windows;
using StructureMap;

namespace StackOverflowException
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        public App()
        {
            Container = new Container(
                x =>
                {
                    x.For<IMainWindow>().Use<MainWindow>();
                    x.For<IMainWindowViewModel>().Use<MainWindowViewModel>();
                });
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var window = (MainWindowViewModel) Container.GetInstance<IMainWindowViewModel>();
            window.WrongPacmanUri = @"D:\VisualStudio\Desktop\StackOverflowException\StackOverflowException\StackOverflowException\Resources\WrongPacman.bmp";
            MainWindow = (MainWindow) window.Window;
            MainWindow.ShowDialog();
        }

        public IContainer Container { get; set; }
    }
}
