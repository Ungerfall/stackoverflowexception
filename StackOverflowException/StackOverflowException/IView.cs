﻿namespace StackOverflowException
{
    public interface IView
    {
        object DataContext { get; set; } 
    }
}