﻿using StructureMap;

namespace StackOverflowException
{
    public class BaseWindowViewModel : BaseNotification, IWindowViewModel
    {
        public BaseWindowViewModel(IWindow window, IContainer container)
        {
            Window = window;
            Window.DataContext = this;
            Container = container;
        }

        private IWindow window;

        public IWindow Window
        {
            get { return window; }
            set
            {
                window = value;
                OnPropertyChanged();
            }
        }

        private IView view;

        public IView View
        {
            get { return view; }
            set
            {
                view = value;
                OnPropertyChanged();
            }
        }

        private IContainer container;

        public IContainer Container
        {
            get { return container; }
            set
            {
                container = value;
                OnPropertyChanged();
            }
        }
    }
}