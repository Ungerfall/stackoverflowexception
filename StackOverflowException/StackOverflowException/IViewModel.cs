﻿namespace StackOverflowException
{
    public interface IViewModel
    {
        IView View { get; set; } 
    }
}